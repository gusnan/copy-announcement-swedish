#!/usr/bin/perl -w

# Copy a english News item and translate as much as possible of it
# to Swedish

# Based on copyadvisory
# Written in 2000-2006 by Peter Karlsson <peterk@debian.org>
# © Copyright 2000-2005 Software in the public interest, Inc.
# This program is released under the GNU General Public License, v2.

# © Copyright 2018 Andreas Rönnquist

use File::Basename;

# Get command line
$infile = $ARGV[0];

$srcdir = dirname($infile);

(my $outfile_full = $infile) =~ s/\/en\//\/sv\//d;

my($destfile, $destdir, $destext) = fileparse($outfile_full, qr/\.[^.]*/);

# Create needed file and directory names
$srcfile= "$srcdir/$destfile$destext";
$dstfile= "$destdir/$destfile$destext";

# Sanity checks
die "File $srcfile does not exist\n"     unless -e $srcfile;
die "File $dstfile already exists\n"     if     -e $dstfile;
# mkdir $dstdir, 0755                      unless -d $dstdir;

open SRC, $srcfile
	or die "Could not read $srcfile ($!)\n";

open DST, ">$dstfile"
	or die "Could not create $dstfile ($!)\n";

# DON'T Insert the revision number
# print DST qq'#use wml::debian::translation-check translation="1.1" mindelta="1"\n';

# Copy the file
while (<SRC>)
{
	next if /\$Id/;

	s/Updated Debian (.*): ([0-9]+)\.([0-9]+) released/Uppdaterad Debian $1\; $2.$3 utgiven/;
	s/\<p\>The Debian project is pleased to announce the (.*) update of its/\<p\>Debianprojektet presenterar stolt sin $1 uppdatering till dess/;
	s/stable distribution Debian \<release\> \(codename \<q\>\<codename\>\<\/q\>\)\./stabila utgåva Debian \<release\> \(med kodnamnet \<q\>\<codename\>\<\/q\>\)\./;
	s/This point release mainly adds corrections for security issues\,/Denna punktutgåva lägger huvudsakligen till rättningar för säkerhetsproblem,/;
	s/along with a few adjustments for serious problems\.  Security advisories/tillsammans med ytterligare rättningar för allvarliga problem\. Säkerhetsbulletiner/;
	s/have already been published separately and are referenced where available\.\<\/p\>/har redan publicerats separat och refereras när de finns tillgängliga\.\<\/p\>/;
	s/\<p\>Please note that the point release does not constitute a new version of Debian/\<p\>Vänligen notera att punktutgåvan inte innebär en ny version av Debian /;
	s/\<release\> but only updates some of the packages included.  There is/\<release\> utan endast uppdaterar några av de inkluderade paketen. Det behövs/;
	s/no need to throw away old \<q\>\<codename\>\<\/q\> media. After installation,/inte kastas bort gamla media av \<q\>\<codename\>\<\/q\>. Efter installationen/;
	s/packages can be upgraded to the current versions using an up-to-date Debian/kan paket uppgraderas till de aktuella versionerna genom att använda en uppdaterad/;
	s/mirror\.\<\/p\>/Debianspegling.\.\<\/p\>/;
	s/\<p\>Those who frequently install updates from security.debian.org won't have/\<p\>Dom som frekvent installerar uppdateringar från security.debian.org kommer inte att behöva/;
	s/to update many packages, and most such updates are/uppdatera många paket, och de flesta av sådana uppdateringar finns/;
	s/included in the point release.\<\/p\>/inkluderade i punktutgåvan\.\<\/p\>/;
	s/\<p\>New installation images will be available soon at the regular locations.\<\/p\>/\<p\>Nya installationsavbildningar kommer snart att finnas tillgängliga på dom vanliga platserna.\<\/p\>/;
	s/\<p\>Upgrading an existing installation to this revision can be achieved by/\<p\>En uppgradering av en existerande installation till denna revision kan utföras genom att/;
	s/pointing the package management system at one of Debian\'s many HTTP mirrors\./peka pakethanteringssystemet på en av Debians många HTTP\-speglingar\./;
	s/A comprehensive list of mirrors is available at\:\<\/p\>/En utförlig lista på speglingar finns på\:\<\/p\>/;
	s/\<h2\>Security Updates\<\/h2\>/\<h2\>Säkerhetsuppdateringar\<\/h2\>/;
    s/\<p\>This revision adds the following security updates to the oldstable release/\<p\>Denna revision lägger till följande säkerhetsuppdateringar till den gamla stabila utgåvan/;
	s/\<p\>This revision adds the following security updates to the stable release/\<p\>Denna revision lägger till följande säkerhetsuppdateringar till den stabila utgåvan/;
	s/\<p\>This revision adds the following security updates to the stable/\<p\>Denna revision lägger till följande säkerhetsuppdateringar till den stabila/;
	s/release\. The Security Team has already released an advisory for each of/utgåvan\. Säkerhetsgruppen har redan givit ut bullentiner för var och en av/;
	s/these updates\:\<\/p\>/dessa uppdateringar\:\<\/p\>/;
	s/\<tr\>\<th\>Advisory ID\<\/th\>/\<tr\>\<th\>Bulletin-ID\<\/th\>/;
	s/\<th\>Package\<\/th>\<\/tr\>/\<th\>Paket\<\/th\>\<\/tr\>/;
	s/\<h2\>Removed packages\<\/h2\>/\<h2\>Borttagna paket\<\/h2\>/;
	s/\<p\>The following packages were removed due to circumstances beyond our/\<p\>Följande paket har tagits bort på grund av omständigheter utom vår/;
	s/control\:\<\/p\>/kontroll\:\<\/p\>/;
	s/\<h2\>Miscellaneous Bugfixes\<\/h2\>/\<h2\>Blandade felrättningar\<\/h2\>/;
    s/\<p\>This oldstable update adds a few important corrections to the following/\<p\>Denna uppdatering av den gamla stabila utgåvan lägger till några viktiga felrättningar till följande/;
	s/\<p\>This stable update adds a few important corrections to the following/\<p\>Denna uppdatering av den stabila utgåvan lägger till några viktiga felrättningar till följande/;
	s/packages\:\<\/p\>/paket\:\<\/p\>/;
	s/\<tr\>\<th\>Package\<\/th\>/\<tr\>\<th\>Paket\<\/th\>/;
	s/\<th\>Reason\<\/th\>\<\/tr\>/<th\>Orsak\<\/th\>\<\/tr\>/;
	s/\<h2\>Debian Installer\<\/h2\>/\<h2\>Debianinstalleraren\<\/h2\>/;
	s/\<p\>The installer has been updated to include the fixes incorporated into/\<p\>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den/;
	s/\<p\>The installer has been updated to include the fixes incorporated/\<p\>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den/;
	s/into stable by the point release.\<\/p\>/stabila utgåvan med denna punktutgåva.\<\/p\>/;
    s/into oldstable by the point release.\<\/p\>/gamla stabila utgåvan med denna punktutgåva.\<\/p\>/;
	s/stable by the point release.\<\/p\>/stabila utgåvan med denna punktutgåva.\<\/p\>/;
	s/\<h2\>URLs\<\/h2\>/\<h2\>URLer\<\/h2\>/;
	s/\<p\>The complete lists of packages that have changed with this/\<p\>Den fullständiga listan på paket som har förändrats i denna/;
	s/revision:\<\/p\>/revision\:\<\/p\>/;
	s/\<p\>The current stable distribution\:\<\/p\>/\<p\>Den aktuella stabila utgåvan\:\<\/p\>/;
    s/\<p\>The current oldstable distribution\:\<\/p\>/\<p\>Den aktuella gamla stabila utgåvan\:\<\/p\>/;
	s/\<p\>Proposed updates to the stable distribution\:\<\/p\>/\<p\>Föreslagna uppdateringar till den stabila utgåvan\:\<\/p\>/;
    s/\<p\>Proposed updates to the oldstable distribution\:\<\/p\>/\<p\>Föreslagna uppdateringar till den gamla stabila utgåvan\:\<\/p\>/;
	s/\<p\>stable distribution information \(release notes\, errata etc\.\)\:\<\/p\>/\<p\>Information om den stabila utgåvan \(versionsfakta\, kända problem osv\.\)\:\<\/p\>/;
    s/\<p\>oldstable distribution information \(release notes\, errata etc\.\)\:\<\/p\>/\<p\>Information om den gamla stabila utgåvan \(versionsfakta\, kända problem osv\.\)\:\<\/p\>/;
	s/\<p\>Security announcements and information\:\<\/p\>/\<p\>Säkerhetsbulletiner och information\:\<\/p\>/;
	s/\<h2\>About Debian\<\/h2\>/\<h2\>Om Debian\<\/h2\>/;
	s/\<p\>The Debian Project is an association of Free Software developers who/\<p\>Debianprojektet är en grupp utvecklare av Fri mjukvara som/;
	s/volunteer their time and effort in order to produce the completely free/donerar sin tid och kraft för att producera det helt fria/;
	s/volunteer their time and effort in order to produce the completely/donerar sin tid och kraft för att producera det helt/;
	s/free operating system Debian\.\<\/p\>/fria operativsystemet Debian\.\<\/p\>/;
	s/operating system Debian\.\<\/p\>/operativsystemet Debian\.\<\/p\>/;
	s/\<h2\>Contact Information\<\/h2\>/\<h2\>Kontaktinformation\<\/h2\>/;
	s/\<p\>For further information, please visit the Debian web pages at \<a/\<p\>För ytterligare information, vänligen besök Debians webbplats på \<a/;
	s/\<p\>For further information, please visit the Debian web pages at/\<p\>För ytterligare information, vänligen besök Debians webbplats på/;
	s/href\=\"\$\(HOME\)\/\"\>https\:\/\/www.debian.org\/\<\/a\>\, send mail to/href\=\"\$\(HOME\)\/\"\>https\:\/\/www.debian.org\/\<\/a\>\, skicka e-post till/;
	s/\&lt\;press\@debian\.org\&gt\;\, or contact the stable release team at/\&lt\;press\@debian\.org\&gt\;\, eller kontakta gruppen för stabila utgåvor på/;
    s/first/första/;
    s/second/andra/;
    s/third/tredje/;
	s/fourth/fjärde/;
	s/fifth/femte/;
	s/sixth/sjätte/;
	s/seventh/sjunde/;
	s/eighth/åttonde/;
	s/ninth/nionde/;
	s/tenth/tionde/;
	s/eleventh/elfte/;
    s/twelvth/tolfte/;
	s/The Security Team has already released an advisory for each of these/Säkerhetsgruppen har redan släppt bulletiner för alla dessa/;
	s/updates\:\<\/p\>/uppdateringar\:\<\/p\>/;

	print DST $_;
}

close SRC;
close DST;

# We're done
print "Copying done, remember to edit $dstfile\n";
